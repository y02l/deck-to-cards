# デッキからカードを検出するサービス

## 依存するサービス


## APIリファレンス
### デッキ情報取得
#### HTTPリクエスト
* POST http://deck-to-cards/

#### リクエストボディ
```
{
  q: base64img #デッキ画像
}
```

#### レスポンス
* カードの順序は左上から右下
```
[
    name1,
    name2,
    ...
]
```
