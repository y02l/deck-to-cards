FROM python:3.8-slim

ENV LIB="libswscale-dev \
  libtbb2 \
  libtbb-dev \
  libjpeg-dev \
  libpng-dev \
  libtiff-dev \
  libglib2.0-0 \
  libsm6 \
  libxext6 \
  libavformat-dev \
  libpq-dev \
  libgl1-mesa-dev"

RUN apt-get update \
  && apt-get -y upgrade \
  && apt-get install --no-install-recommends -qy $LIB \
  && apt-get clean \
  && apt-get autoclean \
  && apt-get autoremove

COPY ./app/requirements.txt /app/
RUN pip install --no-cache-dir -r /app/requirements.txt

RUN rm -rf /tmp/* /var/tmp/* \
  && rm -rf /var/lib/apt/lists/* \
  rm -rf /var/lib/apt/lists/*

COPY ./app /app

# start flask
ENV FLASK_APP='/app/run.py'
ENTRYPOINT ["flask", "run", "--host", "0.0.0.0", "--port", "80"]
