import cv2
import numpy as np
from functools import reduce


def split(img):
    '''
    Parameters
    ----------
    img : cv2.image
      デッキ画像
    
    Returns
    -------
    imgs : [cv2.image]
      カード画像(イラストのみ)
    '''
    # カードの高さの最小ピクセル
    CARD_MIN_HEIGHT = 50

    def opening(img, h, w):
        kernel = np.ones((h, w), np.uint8)
        img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
        return img

    # 雑多な矩形から、カードであろう矩形を選ぶ
    def iscard(h, w):
        # 69 : 113
        return h * 65 < w * 113 and w * 113 < h * 74 and h > CARD_MIN_HEIGHT

    # カードからイラスト部分を切り出す
    def cut_illust(img):
        he, wi = img.shape[:2]
        h, w, h1, w1, h2, w2 = 118, 72, 34, 9, 88, 63
        return img[int(he * h1 / h):int(he * h2 / h),
                   int(wi * w1 / w):int(wi * w2 / w)]

    # カードフレームから、空カードかを判断
    def isnotempty(img):
        he, wi = img.shape[:2]
        w, h, x, y = 69, 115, 58, 6
        pix = img[int(he * y / h), int(wi * x / w)]
        return sum(list(pix)) > 50

    def isnotpool(img):
        he, wi = img.shape[:2]
        w, h, x, y = 81, 140, 38, 13
        find000 = False
        for xx in range(20):
            for yy in range(20):
                pix = img[int(he * (y + yy - 10) / h),
                          int(wi * (x + xx - 10) / w)]
                find000 = find000 or sum(list(pix)) <= 3
        return find000

    def color_mask(hsv, lowerhsv, upperhsv):
        def gimp2cv(hsv):
            h, s, v = hsv
            return [int(h * 180 / 360), int(s * 256 / 100), int(v * 256 / 100)]

        lower = np.array(gimp2cv(lowerhsv))
        upper = np.array(gimp2cv(upperhsv))
        mask = cv2.inRange(hsv, lower, upper)
        return mask

    def detect_good_rect1(hsv):
        blue_background_mask = color_mask(hsv, (210, 50, 0), (230, 100, 70))
        level_y_mask = color_mask(hsv, (40, 0, 0), (65, 100, 100))
        level_r_mask = color_mask(hsv, (0, 70, 30), (10, 100, 100))
        level_b_mask = color_mask(hsv, (0, 0, 0), (0, 0, 0))
        mask = reduce(lambda res, m: cv2.bitwise_or(res, m),
                      [blue_background_mask, level_b_mask])
        element = cv2.bitwise_not(mask)
        opened_element = opening(element, 4, 4)
        return opened_element

    def detect_good_rect2(hsv):
        retual = color_mask(hsv, (210, 40, 70), (220, 60, 80))
        effect = color_mask(hsv, (10, 0, 0), (30, 100, 100))
        normal = color_mask(hsv, (30, 0, 0), (50, 100, 100))
        spell = color_mask(hsv, (160, 0, 0), (180, 100, 100))
        trap = color_mask(hsv, (320, 0, 0), (330, 100, 100))
        tag = color_mask(hsv, (95, 0, 0), (105, 100, 100))
        frame = color_mask(hsv, (190, 0, 50), (250, 50, 100))
        element = reduce(lambda res, m: cv2.bitwise_or(res, m),
                         [retual, effect, normal, spell, trap, tag, frame])
        return element

    # 矩形を浮き出す
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    good_rect = detect_good_rect1(hsv)

    # カードを検出
    contours, hierarchy = cv2.findContours(good_rect, cv2.RETR_TREE,
                                           cv2.CHAIN_APPROX_SIMPLE)
    top_contours = [c for c, h in zip(contours, hierarchy[0]) if h[3] == -1]
    rects = [cv2.boundingRect(c) for c in top_contours]
    card_rect = [rect for rect in rects if iscard(rect[3], rect[2])]

    # ソート
    y_sorted_rect = sorted(card_rect, key=lambda rect: rect[1])
    col_group = [[y_sorted_rect[0]]]
    for rect in y_sorted_rect[1:]:
        if col_group[-1][0][1] + CARD_MIN_HEIGHT > rect[1]:
            col_group[-1].append(rect)
        else:
            col_group.append([rect])
    y_x_sorted_rect = [
        sorted(col, key=lambda rect: rect[0]) for col in col_group
    ]
    sorted_rect = [e for inner_list in y_x_sorted_rect for e in inner_list]

    # 切り出し
    card_imgs_w_empty = [img[y:y + h, x:x + w] for x, y, w, h in sorted_rect]

    # 空カード枠を削除
    card_imgs = [img for img in card_imgs_w_empty if isnotempty(img)]

    # 大きさが離反しているのはカードじゃない
    ave = sum([img.size for img in card_imgs]) / len(card_imgs)
    card_imgs_avesize = [
        img for img in card_imgs if abs(img.size - ave) < ave / 2
    ]
    # pc版右側を除去
    card_imgs_not_pool = [img for img in card_imgs_avesize if isnotpool(img)]

    # イラスト部のみを切り出し
    illust_imgs = [cut_illust(img) for img in card_imgs_not_pool]

    return illust_imgs
