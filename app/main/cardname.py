import requests
import base64
import json

import cv2


def overlay_sample(img):
    '''
    公式カードと比較するためにSAMPLEの文字を上に重ねる
    '''
    return img


def img2base64str(img):
    retval, buffer = cv2.imencode('.png', img)
    png_base64 = base64.b64encode(buffer)
    png_str = png_base64.decode()
    return png_str


def cardname(img):
    '''
    Parameters
    ----------
    img : cv2.image
      カード画像(イラストのみ)
    
    Returns
    -------
    name : str
      カード名
    '''
    resp = requests.post('http://image-search-official-card/search',
                         data=json.dumps({'query': img2base64str(img)}),
                         headers={'content_type': 'application/json'})
    data = resp.json()
    return data[0]['tag']
