import cv2
import os
from glob import glob

from main.split import split

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = BASE_DIR + '/data/'


def test_split():
    for p in glob(DATA_DIR + '/*.data'):
        with open(p, 'r') as f:
            img, names = f.read().splitlines()
            img = cv2.imread(DATA_DIR + '/' + img)
            names = names.split(',')
            assert len(split(img)) == len(names)
