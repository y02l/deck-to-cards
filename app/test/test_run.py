import json
import os
from glob import glob

import pytest
import cv2

from run import app, img2base64str

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
DATA_DIR = BASE_DIR + '/data/'


@pytest.fixture(scope='function', autouse=True)
def prepost():
    app.config['TESTING'] = True
    yield
    pass


def test_post_root():
    client = app.test_client()
    for p in glob(DATA_DIR + '/*.data'):
        with open(p, 'r') as f:
            img, names = f.read().splitlines()
            img = cv2.imread(DATA_DIR + '/' + img)
            names = names.split(',')

        resp = client.post('/',
                           content_type='application/json',
                           data=json.dumps(dict(q=img2base64str(img))))
        data = json.loads(resp.get_data().decode('UTF-8'))
        assert names == data
